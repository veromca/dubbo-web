package com.dubbo.web.cache;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.dubbo.web.common.cache.redis.RedisCachedManagerImpl;
@Component
public class VerifyCodeCache {
    @Resource(name = "dubboWebCache")
    private RedisCachedManagerImpl cache;
    
    /**
     * key
     */
    public static final String KEY_SMS_VERIFYCODE_TELPHONE = "dubbo:web:valifycode:type_telphone:%s_%s";
    
    /**
     * 验证码有效期(秒)
     */
    public static final int SMS_VERIFYCODE_EXPIRE_TIME = 30 * 60;
    
    private static String getKey(String type,String telphone) {
        return String.format(KEY_SMS_VERIFYCODE_TELPHONE,type, telphone);
    }
    /**
     * 
    * @Title: getValifyCode 
    * @Description: 获取验证码 
    * @param @param type
    * @param @param telphone
    * @param @return    设定文件 
    * @return String    返回类型 
    * @throws
     */
    public String getValifyCode(String type, String telphone) {
        if (StringUtils.isBlank(type) || StringUtils.isBlank(telphone)) {
            return null;
        }
        String valifyCode = cache.get(getKey(type, telphone));
        return valifyCode;
    }
    /**
     * 
    * @Title: saveValifyCode 
    * @Description: 保存验证码
    * @param @param type
    * @param @param telphone
    * @param @param valifyCode    设定文件 
    * @return void    返回类型 
    * @throws
     */
    public Boolean saveValifyCode(String type, String telphone, String valifyCode) {
        if (StringUtils.isBlank(type) || StringUtils.isBlank(telphone) || StringUtils.isBlank(valifyCode)) {
            return false;
        }
        return cache.set(getKey(type, telphone), SMS_VERIFYCODE_EXPIRE_TIME, valifyCode.trim());
    }
    /**
     * 
    * @Title: delValifyCode 
    * @Description: 删除缓存验证码
    * @param @param type
    * @param @param telphone
    * @param @return    设定文件 
    * @return Boolean    返回类型 
    * @throws
     */
    public Boolean delValifyCode(String type, String telphone) {
        if (StringUtils.isBlank(type) || StringUtils.isBlank(telphone)) {
            return false;
        }
        return cache.remove(getKey(type, telphone));
    }
    
    public static void main(String[] args) {
        //LogUtils.serviceLog(getKey("kjsadfk","12344566"));
    }
}

