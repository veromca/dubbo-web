/**
 * @Title: UserLoginCtrl.java
 * @Package com.viewhigh.b2bportal.web.user.controller
 * @Description: TODO
 * @author shaoheshan
 * @date 2016年6月16日 下午7:00:38
 * @version V1.0
 */
package com.dubbo.web.controller.system;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.dubbo.model.User;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dubbo.web.cache.VerifyCodeCache;
import com.song.dubboapi.service.system.IUserService;

/**
 * 
* @ClassName: UserCtrl 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author liusongqing
* @date 2016年8月31日 上午9:57:42 
*
 */
@Controller
@RequestMapping("web/system/user")
public class UserCtrl{

    
    @Resource
    private IUserService iUserService;
    
    @Resource
    private VerifyCodeCache verifyCodeCache;
    
    @RequestMapping(value = "/toUserList")
    public ModelAndView toUserList(HttpServletRequest request) {
        //add session cache
        request.getSession().setAttribute("session:cache:userName", "admin");
        System.out.println(request.getSession().getAttribute("session:cache:userName"));
        //add redis cache
        verifyCodeCache.saveValifyCode("wx", "1231232132", "333");
        //get redis cache
        String cacheValue = verifyCodeCache.getValifyCode("wx", "1231232132");
        System.out.println("**************************cache="+cacheValue);
        //send RPC 
        System.out.println(iUserService.sayHello("宝宝"));
        List<User> list = iUserService.getUsers();
        System.out.println(list.get(0).getName());
        ModelAndView mv = new ModelAndView("/system/user/userList");
        return mv;
     }
    
    public static void main( String[] args ) throws IOException
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                new String[] { "bean/dubbo-web-root-bean.xml" });
        context.start();

        IUserService demoService = (IUserService) context.getBean("userService");
        String hello = demoService.sayHello("liusongqing");
        System.out.println(hello);

        List list = demoService.getUsers();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                System.out.println(list.get(i));
            }
        }
        System.in.read();

    }
}
