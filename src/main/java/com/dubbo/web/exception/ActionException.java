package com.dubbo.web.exception;

/**   
* @Title: ActionException.java 
* @Package com.dubbo.web.exception 
* @Description: TODO(用一句话描述该文件做什么) 
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月30日 下午4:47:25 
* @version V1.0   
*/
public class ActionException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = -3848547174252753762L;

    protected boolean isAjax = false;
    public ActionException() {
    }
    public ActionException(int code, String msg) {
        super(msg);
    }

    public ActionException(String message) {
        super(message);
    }

    public ActionException(Throwable cause) {
        super(cause);
    }

    public ActionException(String message, Throwable cause) {
        super(message, cause);
    }

    
  

    public boolean isAjax() {
        return isAjax;
    }

    public void setAjax(boolean isAjax) {
        this.isAjax = isAjax;
    }

    public ActionException setAjax() {
        this.isAjax = true;
        return this;
    }

}

