package com.dubbo.web.exception;/**   
* @Title: CacheException.java 
* @Package com.dubbo.web.exception 
* @Description: TODO(用一句话描述该文件做什么) 
* @author liusongqing 
* @Company www.viewhigh.com
* @date 2016年8月30日 下午4:46:24 
* @version V1.0   
*/
public class CacheException extends ActionException {

    /**
     * 
     */
    private static final long serialVersionUID = -7366217531577789280L;

    public CacheException() {
        super();
    }

    public CacheException(String message, Throwable cause) {
        super(message, cause);
    }

    public CacheException(String message) {
        super(message);
    }

    public CacheException(Throwable cause) {
        super(cause);
    }

}

